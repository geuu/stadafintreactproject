import { useNavigate, Link } from "react-router-dom";
import Navbar from "./components/Navbar";
import "./styles/landingpage.css";
import KidzPlaying from "./assets/icons/image.png";
import diamantPic from "./assets/icons/diamantPic.jpg";
import windowPic from "./assets/icons/windowPic.jpg";
import basicPic from "./assets/icons/basicPic.jpg";
import toppPic from "./assets/icons/toppPic.jpg";
import stadafint_logo from "./assets/icons/stadafint_logo.png";
import { useState } from "react";

function LandingPage() {
  return (
    <>
      <Navbar />

      <main>
        <section className="main-section">
          <div className="info-div">
            <h1>
              GÖR MER TID FÖR <span>LIVET</span>
            </h1>
            <h1>
              VI TAR <span>RÖRAN</span>
            </h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam
              ex quo repellat deleniti illo sequi ducimus exercitationem placeat
              quam quos!
            </p>
            <form>
              <input
                className="gradient-border"
                type="text"
                placeholder="POSTNUMMER"
              />
              <input
                className="gradient-border"
                type="text"
                placeholder="VäLJ TJÄNST"
              />
              <button className="form-btn">GÅ VIDARE</button>
            </form>
          </div>
          <div>
            <img className="header-image" src={KidzPlaying}></img>
          </div>
        </section>

        <section className="page-divider">
          <h2>LOREM IPSUM</h2>
        </section>

        <section className="services">
          <div>
            <h1>Våra tjänster</h1>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit.
              Aspernatur deserunt veritatis adipisci rem rerum explicabo eius
              aut? Explicabo aspernatur natus molestiae debitis consequuntur
              expedita perferendis, ut adipisci, aut maiores quaerat, eaque ad
              illum quas nihil! Harum iste consectetur quod quibusdam provident,
              deleniti veritatis dignissimos dolore, porro, omnis in recusandae
              soluta?
            </p>
          </div>
          <div className="services-categories">
            <div className="upper-categories">
              <div className="services">
                <img className="service-img" src={basicPic}></img>
                <h2>Basic</h2>

                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
                <p>
                  {" "}
                  <a href="">Läs mer...</a>
                </p>
              </div>
              <div className="services">
                <img className="service-img" src={toppPic}></img>
                <h2>Topp</h2>

                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
                <p>
                  {" "}
                  <a href="">Läs mer...</a>
                </p>
              </div>
            </div>
            <div className="lower-categories">
            <div className="services">
                <img className="service-img" src={diamantPic}></img>
                <h2>Diamant</h2>

                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
                <p>
                  {" "}
                  <a href="">Läs mer...</a>
                </p>
              </div>
              <div className="services">
                <img className="service-img" src={windowPic}></img>
                <h2>Fönstertvätt</h2>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
                <p>
                  <a href="">Läs mer...</a>
                </p>
              </div>
            </div>
          </div>
        </section>
      </main>

      <footer>
        <div className="logo-container">
        <Link className="" to={"../"}>
        <img className="logo" src={stadafint_logo}></img>
              </Link>
        </div>

        <div className="footer-1">
          <h2>MENY</h2>
          <a href="#">Om oss</a>
          <a href="#">Kontakt</a>
          <a href="#">Våra städare</a>
          <a href="#">Tjänster</a>
          <a href="#">Blogg</a>
        </div>

        <div className="footer-2">
          <h2>NYHETSBREV</h2>
          <form>
            <input type="email" placeholder="Din email" required />
            <br />
            <button type="submit">Prenumerera</button>
          </form>
        </div>

        <div className="footer-3">
          <h2>KONTAKT</h2>
          <p>
            Renvägen 106 <br /> 286 56, Skövde <br /> +46 70493761
          </p>
        </div>
      </footer>
    </>
  );
}
export default LandingPage;
