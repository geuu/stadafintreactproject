import { useEffect, useState } from "react";
import axios from "axios";
import { Booking, TimeSpan, defaultBooking } from "./types";
import BookingList from "./components/BookingList";
import BookingForm from "./components/BookingForm";
import Navbar from "./components/Navbar";
import "./styles/form.css";
import "./styles/bookingpage.css";

function BookingPage() {
  // State för listan med alla bookings, tsx = <"text"+"typ" i detta fallet en array>("innehåll")
  // typen är en array av typen bookings (alla krav i "Booking" dvs id, date osv måste uppfyllas)
  const [bookings, setBookings] = useState<Booking[]>([]);
  //State för formulärvärdena på nya bookingen, variabel som håller en array av tider som är blockerade pga existerande bokningar
  const [blockedTimes, setBlockedTimes] = useState<TimeSpan[]>([]);
//användarnamnet. By default enligt defaultbookingen sen ändras till den första i db.json
  const [userName,setUserName] = useState<string>(defaultBooking.client)


  //när komponenten BookingPage skapas så körs funktionen
  useEffect(() => {
    fetchBookings();
  },[]);
  // getTime fungerar i millisekunder

  // useEffect körs varje gång "bookings"-state förändras och då
  // ska blockedTimes ändras till att antingen blockera eller lösa upp tiden av bokningen som ändrades

  useEffect(() => {
    const times: TimeSpan[] = bookings
      .filter((booking) => !booking.status)
      .map((booking) => {
        const startDate = new Date(booking.date);
        const stopDate = new Date(startDate.getTime() + 3 * 60 * 60 * 1000);
        return { cleaner: booking.cleaner, from: startDate, to: stopDate };
      });

    setBlockedTimes(times);
  }, [bookings]);

  // Lägger till bokning (som man skickar med som parameter) till server och Bookings-staten
  // ... = destructuring
  // skapar upp tempbooking (kopia) för att generera ett ID. Läggs till på server mha axios
  // setbookings slår ihop tidigare lista med tempbooking
  // kopplar samman clients username med booking
  const addBooking = async (booking: Booking) => {
    try {
      const tempBooking = { ...booking, id: crypto.randomUUID(), client:userName };
      await axios.post("http://localhost:3000/bookings", tempBooking);
      setBookings((prevList) => [...prevList, tempBooking]);
    } catch (error) {
      console.log("proplem med å adde booking", error);
    }
  };

  //  Hämtar bookings från databasen när bookingpage.tsx komponenten laddas in
  const fetchBookings = async () => {
    try {
      const response = await axios.get("http://localhost:3000/bookings");
      setUserName(response.data[0].client)
      setBookings(response.data);
    } catch (error) {
      console.log("Feil ved henting av data", error);
    }
  };

  // Tar bort booking (vars id vi skickar med som parameter) från både server och bookings state
  // filter skapar en ny array med de ID:n som inte är samma som det id:t vi skickar med som parameter.
  const deletePost = async (id: string) => {
    try {
      await axios.delete(`http://localhost:3000/bookings/${id}`);
      setBookings((prevList) => prevList.filter((post) => post.id !== id));
    } catch (error) {
      console.log("feil ved delete fra server", error);
    }
  };

  //funktion för att ändra status i bokning från false till true (både i databas och state)
  const completePost = async (booking: Booking) => {
    //referer til booking med denne id og endrer bare statuset i json fil
    try {
      await axios.put(`http://localhost:3000/bookings/${booking.id}`, {
        ...booking,
        status: true,
      });
      //endrer i arrayen til denne id, prevList = listen vi looper gjennom som er den gamle listen
      setBookings((prevList) => {
        return prevList.map((item) => {
          if (item.id === booking.id) {
            return { ...item, status: true };
          }
          return item;
        });
      });
    } catch (error) {
      console.log("fel");
    }
  };

  return (
    <>
      <Navbar />
      <h1 className="booking-h1">{userName}s Bokningssida</h1>
      <section className="form-section">
        <BookingForm
          addBooking={addBooking}
          blockedTimes={blockedTimes}
        ></BookingForm>
      </section>

      <section className="booking-section">
        <section>
          <h3>Kommande Bokningar</h3>
          <BookingList
            key="Upcoming Bookings"
            isComplete={false}
            bookings={bookings}
            deletePost={deletePost}
            completePost={completePost}
          ></BookingList>
        </section>

        <section>
          <h3>Utförda Bokningar</h3>
          <BookingList
            key="Completed Bookings"
            isComplete={true}
            bookings={bookings}
            deletePost={deletePost}
            completePost={completePost}
          ></BookingList>
        </section>
      </section>
    </>
  );
}

export default BookingPage;

// Denna komponenten sköter statet av bokningar, interagerar med servern för CRUD och renderar UI komponenter för att visa och hantera bokningar
// CRUD = create, read, update, delete